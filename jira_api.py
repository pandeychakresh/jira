import requests
from requests.auth import HTTPBasicAuth
import json
import logging
import sys

##### Default Logging Configuration

slogformat = "%(asctime)s | %(levelname)s | %(message)s"
logging.basicConfig(level=logging.INFO, format=slogformat, stream=sys.stdout)
logging.getLogger("requests").setLevel(logging.INFO)

class jira:

    def __init__(self, url, auth, ssl_verify=True, allow_redirects=True):
        self.url = url
        self.auth = HTTPBasicAuth(auth[0], auth[1])
        self.verify = ssl_verify
        self.allow_redirects = allow_redirects
        self.issueID = None

    def getCompleteUrl(self, operation, issueID=None):

        if not issueID:
            if not self.issueID:
                logging.error("JIRA IssueID is required!!")
                return False
            issueID = self.issueID

        if operation == "comment":
            return "{}/rest/api/2/issue/{}/comment".format(self.url, issueID)
        elif operation == "getTransition":
            return "{}/rest/api/2/issue/{}/transitions".format(self.url, issueID)
        elif operation == "updateTransition":
            return "{}/rest/api/2/issue/{}/transitions?expand=transitions.fields".format(self.url, issueID)
        elif operation == "addAttachment":
            return "{}/rest/api/2/issue/{}/attachments".format(self.url, issueID)
        else:
            return False

    def addNewComment(self, comment, issueID=None):

        url = self.getCompleteUrl("comment", issueID)

        if not url:
            return False

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }

        payload = {
          "body": comment
        }

        payload = json.dumps(payload)

        response = requests.post(
            url,
            data=payload,
            headers=headers,
            auth=self.auth,
            verify=self.verify,
            allow_redirects=self.allow_redirects
        )

        try:
            response.raise_for_status()
        except Exception as ex:
            logging.error("Unable to Make Comment -> " + str(ex))
            return False

        return True


    def getTransition(self, issueID=None):

        url = self.getCompleteUrl("getTransition", issueID)

        if not url:
            return False

        response = requests.get(
            url,
            auth=self.auth,
            verify=self.verify,
            allow_redirects=self.allow_redirects
        )

        try:
            response.raise_for_status()
        except Exception as ex:
            logging.error("Unable to Make Comment -> " + str(ex))
            return False

        return response.json()

    def updateTransition(self, transitionId, comment=None, resolutionName=None, issueID=None):

        url = self.getCompleteUrl("updateTransition", issueID)

        if not url:
            return False

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }

        payload = {
            "update": {
                "comment": [
                    {
                        "add": {
                            "body": comment
                        }
                    }
                ]
            },
            "transition": {
                "id": transitionId
            }
        }

        if resolutionName:
            payload['fields'] = {
                "resolution": {
                    "name": resolutionName
                }
            }

        payload = json.dumps(payload)

        response = requests.post(
            url,
            auth=self.auth,
            headers=headers,
            data=payload,
            verify=self.verify,
            allow_redirects=self.allow_redirects
        )

        try:
            response.raise_for_status()
        except Exception as ex:
            logging.error("Unable to change Status -> " + str(ex))
            return False

        if response.status_code == 200:
            return True
        return False

    def addAttachment(self, filePath, issueID=None):

        try:

            fileObj = open(filePath, "rb")

        except Exception as e:

            logging.error(e)
            return False

        url = self.getCompleteUrl("addAttachment", issueID)

        file = {"file": fileObj}

        headers = {'X-Atlassian-Token': 'no-check', 'Accept': 'application/json'}

        r = requests.post(url, auth=self.auth, files=file, verify=self.verify, headers=headers)

        try:

            r.raise_for_status()

        except Exception as e:

            logging.error("Unable to add attachment --> {}".format(e))
            return False

        finally:

            fileObj.close()

        return True